package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RedHotClams implements Clams {

    public String toString() {
        return "Red Hot Clams from Red Hot Island";
    }
}
