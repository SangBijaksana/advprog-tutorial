package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                                   String visitor, Model model) {
        if (visitor != null && !visitor.equals("")) {
            model.addAttribute("title", visitor + ", I hope you interested to hire me");
        } else {
            model.addAttribute("title", "This is my CV");
        }
        model.addAttribute("name","Aldi Hilman Ramadhani");
        model.addAttribute("birthInfo","Manado, 24 Desember 1999");
        model.addAttribute("address",
                "Tangerang Selatan, Banten");
        model.addAttribute("sd","SDI Al-Azhar BSD");
        model.addAttribute("sdYear","2005-2011");
        model.addAttribute("smp","SMPI Al-Azhar BSD");
        model.addAttribute("smpYear","2011-2013");
        model.addAttribute("sma","SMA Negeri 2 Tangsel");
        model.addAttribute("smaYear","2013-2016");
        model.addAttribute("univ","University of Indonesia");
        model.addAttribute("univYear","2016-Now");
        return "cv";
    }
}
