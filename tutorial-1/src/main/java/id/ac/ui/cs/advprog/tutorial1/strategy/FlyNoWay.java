package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    // TODO Complete me!
    public void fly() {
        System.out.println("Sorry, but I haven't learn to fly, yet.");
    }
}
